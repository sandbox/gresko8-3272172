Commerce cart notifications
----------------------------------

Allows to configure email notifications for abandoned carts (authenticated users only).

This module adds a new Cart notification entity type which is generated and sent
according to the bundle settings.

Visit /admin/commerce/config/orders/cart_notification_type to configure
cart notifications.
