<?php

namespace Drupal\commerce_cart_notifications;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Cart notification entities.
 *
 * @ingroup commerce_cart_notifications
 */
class CartNotificationListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['order_id'] = $this->t('Order ID');
    $header['message_subject'] = $this->t('Subject');
    $header['message_text'] = $this->t('Text');
    $header['created'] = $this->t('Date');
    return $header;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\commerce_cart_notifications\Entity\CartNotification $entity */
    $row['id'] = $entity->id();
    $row['order_id'] = $entity->getOrderId();
    $row['message_subject'] = $entity->getMessageSubject();
    $message_text_value = $entity->get('message_text')->getValue();
    $message_text_array = [
      '#type' => 'processed_text',
      '#text' => $message_text_value[0]['value'],
      '#format' => $message_text_value[0]['format'],
    ];
    $row['message_text'] = \Drupal::service('renderer')->render($message_text_array);
    $row['created'] = \Drupal::service('date.formatter')->format($entity->getCreatedTime(), 'medium');
    return $row;
  }

}
