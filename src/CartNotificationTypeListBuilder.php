<?php

namespace Drupal\commerce_cart_notifications;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of Cart notification type entities.
 */
class CartNotificationTypeListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Cart notification type');
    $header['id'] = $this->t('Machine name');
    $header['status'] = $this->t('Status');
    $header['message_delay'] = $this->t('Message delay');
    $header['description'] = $this->t('Description');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\commerce_cart_notifications\Entity\CartNotificationType $entity */
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['status'] = $entity->isActive() ? $this->t('Enabled') : $this->t('Disabled');
    $row['message_delay'] = $this->t('@delay hours', ['@delay' => $entity->getMessageDelay()]);
    $row['description'] = $entity->getDescription();
    return $row + parent::buildRow($entity);
  }

}
