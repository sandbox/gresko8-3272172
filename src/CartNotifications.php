<?php

namespace Drupal\commerce_cart_notifications;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountSwitcherInterface;
use Drupal\Core\Utility\Token;
use Drupal\user\Entity\Role;
use Drupal\user\Entity\User;

class CartNotifications {

  protected $entity_type_manager;

  protected $database;

  protected $mail_manager;

  protected $renderer;

  protected $token;

  protected $account_switcher;

  public function __construct(EntityTypeManagerInterface $entity_type_manager, Connection $database, MailManagerInterface $mail_manager,
                              RendererInterface $renderer, Token $token, AccountSwitcherInterface $account_switcher) {
    $this->entity_type_manager = $entity_type_manager;
    $this->database = $database;
    $this->mail_manager = $mail_manager;
    $this->renderer = $renderer;
    $this->token = $token;
    $this->account_switcher = $account_switcher;
  }

  /**
   * Sends notifications for all active notification types.
   */
  public function sendNotifications($limit = 20) {
    $notification_types = $this->entity_type_manager->getStorage('cart_notification_type')->loadByProperties(['status' => 1]);

    /** @var \Drupal\commerce_cart_notifications\Entity\CartNotificationType $notification_type */
    foreach ($notification_types as $notification_type) {

      $subject = $notification_type->getMessageSubject();
      $message_text = $notification_type->getMessageText();

      $message_delay = $notification_type->getMessageDelay();
      $message_delay_seconds = $message_delay*3600;
      $cart_age_limit = time() - $message_delay_seconds;

      $notified_orders = $this->database->select('cart_notification', 'n')
        ->fields('n', ['order_id'])
        ->condition('n.type', $notification_type->id())
        ->execute()
        ->fetchCol(0);

      // Get all cart ids along with email addresses, which should be notified
      $query = $this->database->select('commerce_order', 'o');
      $query->fields('o', ['order_id', 'mail']);
      $query->addField('s', 'langcode');
      $query->leftJoin('cart_notification', 'n', 'o.order_id=n.order_id');
      $query->leftJoin('commerce_store', 's', 'o.store_id=s.store_id');
      $query->condition('o.cart', 1);

      // Only notify authenticated users
      $query->condition('o.uid', 0, '>');

      $query->condition('o.changed', $cart_age_limit, '<=');
      $query->condition('o.mail', NULL, 'IS NOT NULL');

      if (!empty($notified_orders)) {
        $query->condition('o.order_id', $notified_orders, 'NOT IN');
      }

      if (!empty($limit)) {
        // Limit sent notifications to avoid request timeout
        $query->range(0, $limit);
      }

      $result = $query->execute()->fetchAllAssoc('order_id', \PDO::FETCH_ASSOC);
      // Send an email for each cart and log the sent message
      foreach ($result as $record) {
        $params = [];

        /** @var \Drupal\commerce_order\Entity\Order $order */
        $order = $this->entity_type_manager->getStorage('commerce_order')->load($record['order_id']);

        // Switch to the owner account for the order tokens to work
        $this->account_switcher->switchTo($order->getCustomer());

        $params['subject'] = $subject;
        $message_array = [
          '#type' => 'processed_text',
          '#text' => $this->token->replace($message_text['value'], ['commerce_order' => $order], ['clear' => TRUE]),
          '#format' => $message_text['format'],
        ];
        $params['message'] = $this->renderer->render($message_array);

        $this->mail_manager->mail('commerce_cart_notifications', 'cart_notification', $record['mail'], $record['langcode'], $params);
        /** @var \Drupal\commerce_cart_notifications\Entity\CartNotification $notification */
        $notification = $this->entity_type_manager->getStorage('cart_notification')->create([
          'type' => $notification_type->id(),
          'order_id' => $record['order_id'],
        ]);
        $notification->save();
        $this->account_switcher->switchBack();
      }
    }
  }

  public function deleteOrderNotifications($order_id) {
    $notifications = $this->entity_type_manager->getStorage('cart_notification')->loadByProperties([
      'order_id' => $order_id
    ]);
    foreach ($notifications as $notification) {
      $notification->delete();
    }
  }
}
