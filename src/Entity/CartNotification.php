<?php

namespace Drupal\commerce_cart_notifications\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Cart notification entity.
 *
 * @ingroup commerce_cart_notifications
 *
 * @ContentEntityType(
 *   id = "cart_notification",
 *   label = @Translation("Cart notification"),
 *   bundle_label = @Translation("Cart notification type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\commerce_cart_notifications\CartNotificationListBuilder",
 *     "views_data" = "Drupal\commerce_cart_notifications\Entity\CartNotificationViewsData",
 *
 *     "access" = "Drupal\commerce_cart_notifications\CartNotificationAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\commerce_cart_notifications\CartNotificationHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "cart_notification",
 *   translatable = FALSE,
 *   admin_permission = "administer cart notification entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "collection" = "/admin/commerce/cart-notifications",
 *   },
 *   bundle_entity_type = "cart_notification_type",
 *   field_ui_base_route = "entity.cart_notification_type.edit_form"
 * )
 */
class CartNotification extends ContentEntityBase implements CartNotificationInterface {

  use EntityChangedTrait;
  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  public function getOrderId() {
    return $this->get('order_id')->value;
  }

  public function getMessageSubject() {
    return $this->get('message_subject')->value;
  }

  public function getMessageText() {
    return $this->get('message_text')->value;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    // Add the published field.
    $fields += static::publishedBaseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Cart notification entity.'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['status']->setDescription(t('A boolean indicating whether the Cart notification is published.'))
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['order_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Order ID'))
      ->setDescription(t('The id of the order (cart) this notification belongs to.'))
      ->setReadOnly(TRUE)
      ->setSetting('unsigned', TRUE);

    $fields['message_subject'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Message subject'))
      ->setDescription(t('The subject of the cart notification message.'))
      ->setSettings([
        'max_length' => 256,
        'text_processing' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['message_text'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Message text'))
      ->setDescription(t('The text of the cart notification message.'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  public static function preCreate(EntityStorageInterface $storage, array &$values) {

    $bundle_entity_type = $storage->getEntityType()->getBundleEntityType();
    if (!empty($values['type'])) {
      $entity_type_manager = \Drupal::entityTypeManager();
      /** @var \Drupal\commerce_cart_notifications\Entity\CartNotificationType $type */
      $type = $entity_type_manager->getStorage($bundle_entity_type)->load($values['type']);
      if (empty($values['message_subject'])) {
        $values['message_subject'] = $type->getMessageSubject();
      }
      if (empty($values['message_text'])) {
        // @todo: move token substitution to presave
        $token_arguments = [];

        /** @var \Drupal\Core\Session\AccountSwitcherInterface $account_switcher */
        $account_switcher = \Drupal::service('account_switcher');

        if (!empty($values['order_id'])) {
          /** @var \Drupal\commerce_order\Entity\Order $order */
          $order = $entity_type_manager->getStorage('commerce_order')->load($values['order_id']);
          $token_arguments['commerce_order'] = $order;

          // Switch to the owner account for the order tokens to work
          $account_switcher->switchTo($order->getCustomer());
        }

        $message_text = $type->getMessageText();
        $message_text['value'] = \Drupal::token()->replace($message_text['value'],
          $token_arguments, ['clear' => TRUE]);
        $values['message_text'] = $message_text;
        $account_switcher->switchBack();
      }
    }

    parent::preCreate($storage, $values);
  }

}
