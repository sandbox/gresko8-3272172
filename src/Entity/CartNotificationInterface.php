<?php

namespace Drupal\commerce_cart_notifications\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;

/**
 * Provides an interface for defining Cart notification entities.
 *
 * @ingroup commerce_cart_notifications
 */
interface CartNotificationInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Cart notification name.
   *
   * @return string
   *   Name of the Cart notification.
   */
  public function getName();

  /**
   * Sets the Cart notification name.
   *
   * @param string $name
   *   The Cart notification name.
   *
   * @return \Drupal\commerce_cart_notifications\Entity\CartNotificationInterface
   *   The called Cart notification entity.
   */
  public function setName($name);

  /**
   * Gets the Cart notification creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Cart notification.
   */
  public function getCreatedTime();

  /**
   * Sets the Cart notification creation timestamp.
   *
   * @param int $timestamp
   *   The Cart notification creation timestamp.
   *
   * @return \Drupal\commerce_cart_notifications\Entity\CartNotificationInterface
   *   The called Cart notification entity.
   */
  public function setCreatedTime($timestamp);

}
