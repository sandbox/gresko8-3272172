<?php

namespace Drupal\commerce_cart_notifications\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Cart notification type entity.
 *
 * @ConfigEntityType(
 *   id = "cart_notification_type",
 *   label = @Translation("Cart notification type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\commerce_cart_notifications\CartNotificationTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\commerce_cart_notifications\Form\CartNotificationTypeForm",
 *       "edit" = "Drupal\commerce_cart_notifications\Form\CartNotificationTypeForm",
 *       "delete" = "Drupal\commerce_cart_notifications\Form\CartNotificationTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\commerce_cart_notifications\CartNotificationTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "cart_notification_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "cart_notification",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/commerce/config/orders/cart_notification_type/{cart_notification_type}",
 *     "add-form" = "/admin/commerce/config/orders/cart_notification_type/add",
 *     "edit-form" = "/admin/commerce/config/orders/cart_notification_type/{cart_notification_type}/edit",
 *     "delete-form" = "/admin/commerce/config/orders/cart_notification_type/{cart_notification_type}/delete",
 *     "collection" = "/admin/commerce/config/orders/cart_notification_type"
 *   },
 *   config_export = {
 *     "label",
 *     "id",
 *     "description",
 *     "status",
 *     "message_subject",
 *     "message_text",
 *     "message_delay",
 *   }
 * )
 */
class CartNotificationType extends ConfigEntityBundleBase implements CartNotificationTypeInterface {

  /**
   * The Cart notification type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Cart notification type label.
   *
   * @var string
   */
  protected $label;

  /**
   * A brief description of this cart notification type.
   *
   * @var string
   */
  protected $description;

  /**
   * The status of this cart notification type.
   *
   * @var boolean
   */
  protected $status;

  /**
   * The time in hours that must pass from cart creation before the notification is sent.
   *
   * @var float
   */
  protected $message_delay;

  /**
   * The subject of the cart notification message.
   *
   * @var string
   */
  protected $message_subject;

  /**
   * The text of the cart notification message.
   *
   * @var string
   */
  protected $message_text;

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function isActive() {
    return $this->status;
  }

  /**
   * {@inheritdoc}
   */
  public function getMessageDelay() {
    return $this->message_delay;
  }

  /**
   * {@inheritdoc}
   */
  public function getMessageSubject() {
    return $this->message_subject;
  }

  /**
   * {@inheritdoc}
   */
  public function getMessageText() {
    return $this->message_text;
  }

}
