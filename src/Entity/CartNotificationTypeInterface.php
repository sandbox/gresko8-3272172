<?php

namespace Drupal\commerce_cart_notifications\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Cart notification type entities.
 */
interface CartNotificationTypeInterface extends ConfigEntityInterface {

  /**
   * Gets the description.
   *
   * @return string
   *   The description of this cart notification type.
   */
  public function getDescription();

  /**
   * Checks if the notification type is active.
   *
   * @return boolean
   *   Notification type status.
   */
  public function isActive();

  /**
   * Gets the time in hours that must pass from cart creation before the notification is sent.
   *
   * @return float
   *   The time in hours that must pass from cart creation before the notification is sent.
   */
  public function getMessageDelay();

  /**
   * Gets the message subject.
   *
   * @return string
   *   The subject of the notification message.
   */
  public function getMessageSubject();

  /**
   * Gets the message text.
   *
   * @return array
   *   The formatted text of the notification message.
   */
  public function getMessageText();
}
