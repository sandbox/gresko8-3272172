<?php

namespace Drupal\commerce_cart_notifications\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Cart notification entities.
 */
class CartNotificationViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
