<?php

namespace Drupal\commerce_cart_notifications\EventSubscriber;

use Drupal\commerce_cart_notifications\CartNotifications;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class OrderEventSubscriber implements EventSubscriberInterface {

  protected $cart_notifications;

  public function __construct(CartNotifications $cart_notifications) {
    $this->cart_notifications = $cart_notifications;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      'commerce_order.place.pre_transition' => 'deleteCartNotifications',
    ];
    return $events;
  }

  /**
   * Deletes the cart notifications for this order.
   */
  public function deleteCartNotifications(WorkflowTransitionEvent $event) {
    $order = $event->getEntity();
    $this->cart_notifications->deleteOrderNotifications($order->id());
  }
}
