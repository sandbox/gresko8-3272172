<?php

namespace Drupal\commerce_cart_notifications\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class CartNotificationTypeForm.
 */
class CartNotificationTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {

    $form = parent::form($form, $form_state);

    /** @var \Drupal\commerce_cart_notifications\Entity\CartNotificationType $type */
    $type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $type->label(),
      '#description' => $this->t("Label for the Cart notification type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\commerce_cart_notifications\Entity\CartNotificationType::load',
      ],
      '#disabled' => !$type->isNew(),
    ];

    $form['description'] = [
      '#title' => $this->t('Description'),
      '#type' => 'textarea',
      '#default_value' => $type->getDescription(),
    ];

    $form['status'] = [
      '#title' => $this->t('Active'),
      '#type' => 'checkbox',
      '#default_value' => $type->isActive(),
      '#description' => $this->t('If enabled, this cart notification type will send notifications.')
    ];

    $form['message_delay'] = [
      '#title' => $this->t('Message delay (hours)'),
      '#description' => $this->t('The time in hours that must pass from cart creation before the notification is sent.'),
      '#type' => 'number',
      '#step' => 0.01,
      '#min' => 0,
      '#default_value' => $type->getMessageDelay(),
      '#required' => TRUE,
    ];

    $form['message_subject'] = [
      '#title' => $this->t('Message subject'),
      '#type' => 'textfield',
      '#default_value' => $type->getMessageSubject(),
      '#required' => TRUE,
    ];

    $message_text = $type->getMessageText();
    $form['message_text'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Message text'),
      '#description' => $this->t('You may use tokens in this field.'),
      '#default_value' => $message_text ? $message_text['value'] : NULL,
      '#format' => $message_text ? $message_text['format'] : NULL,
      '#required' => TRUE,
    ];

    $form['token_browser'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => ['commerce_order'],
      '#click_insert' => FALSE,
      '#dialog' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $type = $this->entity;
    $status = $type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Cart notification type.', [
          '%label' => $type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Cart notification type.', [
          '%label' => $type->label(),
        ]));
    }
    $form_state->setRedirectUrl($type->toUrl('collection'));
  }

}
